**Discount Engine**

This discount Engine can be integrated with any business solution which needs to dynamically provide discount depending on some condition.
As of now, few conditions and discounts are added in logic. Each condition and discount can be bundled together to define a rule.
Both condition and discount definition can be moved to DB and rules can be altered dynamically. 

Key features
1) Used MVEL expression to dynamically evaluate condition and discount. Refer: ConditionDefinition and DiscountEvaluators
2) Key Interfaces: ICondition, IDiscount, IRule, IRuleEngine, IUser
3) Rule engine logic is defined in **RuleEngine.java**. This is **heart** of this engine and will evaluate condition, calculate discount and update order with final total after applying discount.
4) TODO: Following are open points: <br />
4a) Add Logging framework. <br />
4b) Have used Discount and Condition factories which reads hard coded rules. This should be changed and same should be picked from from DB.
5) UML diagram: DiscountEngine.jpg
6) Test cases are added for each condition in RuleEngineTest.java. Main.java - To test each rule, we can use this class.
7) This engine has concept of grouping similar discount together. Only best discount will be applied to final order value.

Requirement: Java 8, Maven 3.3.*
Key Maven Dependencies: org.mvel.mvel2, junit.junit, cobertura-maven-plugin

**Useful Commands**
 <br />To Build:  <br />
mvn clean install

To Run Test:  <br />
mvn test

To generate code coverage report:  <br /> 
mvn clean install <br />
mvn site  <br />
(This would generate report at location: /target/site/cobertura/index.html (as of now, RuleEngine coverage 100%))
