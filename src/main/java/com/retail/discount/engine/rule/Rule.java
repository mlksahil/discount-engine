package com.retail.discount.engine.rule;

import com.retail.discount.engine.core.ICondition;
import com.retail.discount.engine.core.IRule;
import com.retail.discount.engine.dto.Discount;
import com.retail.discount.engine.dto.Order;

/**
 * Rule Definition holder.
 * 
 * @author Sahil Malik
 *
 */
public class Rule implements IRule<Order> {
	
	private ICondition<Order> condition;
	private Discount discount;

	@Override
	public void ruleDefinition(ICondition<Order> condition, Discount discount) {
		this.condition = condition;
		this.discount = discount;
	}

	@Override
	public Discount getDiscountDef() {
		return discount;
	}

	@Override
	public ICondition<Order> getCondition() {
		return condition;
	}

}
