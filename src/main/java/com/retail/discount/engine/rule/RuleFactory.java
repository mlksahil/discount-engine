package com.retail.discount.engine.rule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.retail.discount.engine.core.IRule;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.enums.DiscountType;

/**
 * Rule factory class used to group similar rules.
 * 
 * Rule factory should pulled rule definition from DB. 
 * 
 * @author Sahil Malik
 *
 */
public final class RuleFactory {
	private static RuleFactory factory = new RuleFactory();
	// Rules are grouped. Only one best rule can be applied from a group.
	private Map<DiscountType,List<IRule<Order>>> ruleGroup;

	private RuleFactory() {
		ruleGroup = new HashMap<>();
	}
	
	public static RuleFactory getRuleFactory() {
		return factory;
	}
	
	public void clear() {
		ruleGroup = new HashMap<>();
	}
	
	public void init(List<IRule<Order>> activeRules) {
		for (IRule<Order> iRule : activeRules) {
			List<IRule<Order>> ruleList = ruleGroup.get(iRule.getDiscountDef().getDiscountType());
			if (ruleList == null) {
				ruleList = new ArrayList<>();
				ruleGroup.put(iRule.getDiscountDef().getDiscountType(), ruleList);
			}
			ruleList.add(iRule);
		}
	}

	/**
	 * @return the ruleGroup
	 */
	public Map<DiscountType, List<IRule<Order>>> getRuleGroup() {
		return ruleGroup;
	}
}