package com.retail.discount.engine.rule;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.retail.discount.engine.core.IDiscount;
import com.retail.discount.engine.core.IRule;
import com.retail.discount.engine.core.IRuleEngine;
import com.retail.discount.engine.discount.DiscountFactory;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.enums.DiscountType;

/**
 * Rule Engine. Evaluates condition in rule, if condition is met, apply discount.
 * Current logic apply, all discount on sub-total. This can be altered as per requirement.
 * 
 * @author Sahil Malik
 *
 */
public class RuleEngine implements IRuleEngine {
	
	public void applyDiscount(Order order) {
		BigDecimal cumulativeDiscount = new BigDecimal(0);
		order.setDiscount(new BigDecimal(0));
		order.setTotal(order.getSubTotal());
		try {
			Map<DiscountType, List<IRule<Order>>> ruleGroup = RuleFactory.getRuleFactory().getRuleGroup();
			
			for (Entry<DiscountType, List<IRule<Order>>> group : ruleGroup.entrySet()) {
				DiscountType key = group.getKey();
				System.out.println("Evaluating Discount for Type" + key);
				List<IRule<Order>> value = group.getValue();
				
				BigDecimal bestDiscount = null;
				
				for (IRule<Order> iRule : value) {
					// If rule is success
					if (iRule.getCondition().evaluate(order)) {
						IDiscount<Order> discountEval = DiscountFactory.getInstance().getDiscountEval(iRule.getDiscountDef().getDiscountType());
						BigDecimal calculatedDiscount = discountEval.calculateDiscount(order, iRule.getDiscountDef());
						bestDiscount = bestDiscount == null ? calculatedDiscount : ((calculatedDiscount.subtract(bestDiscount).floatValue()) > 0.0 ? calculatedDiscount : bestDiscount);
					}
				}
				cumulativeDiscount = bestDiscount != null ? cumulativeDiscount.add(bestDiscount) : cumulativeDiscount;
			}
			
			order.setDiscount(cumulativeDiscount);
			BigDecimal updateTotal = order.getSubTotal().subtract(cumulativeDiscount);
			// Update total to 0 if discount is more than total after discount. This should be an error in configuration.
			updateTotal = updateTotal.floatValue() > 0.0 ? updateTotal : new BigDecimal(0.0);
			order.setTotal(updateTotal);
		} catch (Exception e) {
			System.err.println("Error in eval" + e);
			throw e;
		}
	}

}
