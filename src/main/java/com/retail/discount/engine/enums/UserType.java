package com.retail.discount.engine.enums;

/**
 * User Type
 * 
 * @author Sahil Malik
 *
 */
public enum UserType {
	CUSTOMER,
	EMPLOYEE,
	AFFILIATE,
	ADMIM
}
