/**
 * 
 */
package com.retail.discount.engine.enums;

/**
 * Item Type
 * 
 * @author Sahil Malik
 *
 */
public enum ItemType {
	GROCERY,
	PERSONAL_CARE,
	HOUSE_HOLD_NEEDS,
	HOME_KITCHEN
}
