/**
 * 
 */
package com.retail.discount.engine.enums;

/**
 * Discount Type
 * 
 * @author Sahil Malik
 *
 */
public enum DiscountType {
	PERCENT_DISCOUNT,
	FLAT_DISCOUNT,
	EVERY_DOLLAR_DISCOUNT,
	PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE
}
