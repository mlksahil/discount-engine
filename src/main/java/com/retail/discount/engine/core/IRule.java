package com.retail.discount.engine.core;

import com.retail.discount.engine.dto.Discount;

/**
 *
 * Rules Interface
 * 
 * @author Sahil Malik
 *
 */
public interface IRule<T> {
	public void ruleDefinition(ICondition<T> condition, Discount discount);
	public Discount getDiscountDef();
	public ICondition<T> getCondition();
}
