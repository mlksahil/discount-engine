package com.retail.discount.engine.core;

/**
 * Condition Interface
 * 
 * @author Sahil Malik
 *
 */
@FunctionalInterface
public interface ICondition<T> {
	public boolean evaluate(T t);
}
