/**
 * 
 */
package com.retail.discount.engine.core;

import com.retail.discount.engine.dto.Order;

/**
 * Rule Engine Interface
 * 
 * @author Sahil Malik
 *
 */
public interface IRuleEngine {
	
	public void applyDiscount(Order order);

}
