/**
 * 
 */
package com.retail.discount.engine.core;

import java.math.BigDecimal;

import com.retail.discount.engine.dto.Discount;

/**
 * Discount Functional Interface
 * 
 * @author Sahil Malik
 *
 */
@FunctionalInterface
public interface IDiscount<T> {
	public BigDecimal calculateDiscount(T t, Discount discountDef);
}
