package com.retail.discount.engine.core;

import java.util.Calendar;

import com.retail.discount.engine.enums.UserType;

/**
 * User Interface 
 * 
 * @author Sahil Malik
 *
 */
public interface IUser {

	public Long getId();
	public void setId(Long id);
	public String getName();
	public void setName(String name); 
	public UserType getUserType();
	public void setUserType(UserType userType); 
	public String getExternalId();
	public void setExternalId(String externalId);
	public String getEmail();
	public void setEmail(String email); 
	public String getPhoneNo();
	public void setPhoneNo(String phoneNo);
	public Calendar getRegistrationDate();
	public void setRegistrationDate(Calendar registrationDate);
}
