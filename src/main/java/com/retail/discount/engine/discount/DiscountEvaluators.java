package com.retail.discount.engine.discount;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.mvel2.MVEL;

import com.retail.discount.engine.core.IDiscount;
import com.retail.discount.engine.dto.Discount;
import com.retail.discount.engine.dto.Item;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.enums.ItemType;

/**
 * 
 * Dynamic Discount Evaluator
 * 
 * Benefit of using MVEL is, discount string should be externalize to a DB with other required attributes to make it dynamic.
 * 
 * @author Sahil Malik
 *
 */
public interface DiscountEvaluators {
	IDiscount<Order> FLAT_PERCENT_DISCOUNT = (Order o, Discount discount) -> evaluator(o, discount, "order.subTotal.floatValue * discount.discountValue.floatValue");
	IDiscount<Order> EVERY_DOLLAR_DISCOUNT = (Order o, Discount discount) -> evaluator(o, discount, "discount.discountValue.floatValue * (order.subTotal.divide(discount.discountInterval)).intValue");
	IDiscount<Order> PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE = (Order o, Discount discount) -> {
		// TODO Change Item Type. It is hard coded to Grocery for simplification. Ideally, we should externalize expression and dynamic attributes to DB. 
		BigDecimal filterCartSubTotal = o.getCart().stream().filter(i -> !ItemType.GROCERY.equals(i.getItemType())).map(Item::getPrice).reduce(BigDecimal::add).get();
		return evaluator(o, discount, filterCartSubTotal.toString() + " * discount.discountValue.floatValue");
	};
	
	
	
	/**
	 * Static Method to evaluate Discount Expression using MVEL
	 * NOTE: Benefit of using MVEL is, discount string can be externalize to a DB and condition can be made dynamic.
	 * 
	 * @param order Order
	 * @param expression Expression
	 * @return True if expression is good
	 */
	public static BigDecimal evaluator(Order order, Discount discount, String expression) {
		try {
			Map<String,Object> attributes = new HashMap<>();
			attributes.put("discount", discount);
			attributes.put("order", order);
			Serializable mvelExpression = MVEL.compileExpression(expression);
			return new BigDecimal(MVEL.executeExpression(mvelExpression, attributes,Long.class));
		} catch (Exception e) {
			System.err.println("Error evaluating Rule " + e);
			return new BigDecimal(0);
		}
	}
}
