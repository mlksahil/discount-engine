package com.retail.discount.engine.discount;

import java.util.HashMap;
import java.util.Map;

import com.retail.discount.engine.core.IDiscount;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.enums.DiscountType;

/**
 * Discount Factory. Maps Discount Type to Discount Eval implementation.

 * @author Sahil Malik
 *
 */
public final class DiscountFactory {
	private static DiscountFactory factory = new DiscountFactory();
	private Map<DiscountType,IDiscount<Order>> discountTypeToEvalMapping;
	
	public static DiscountFactory getInstance() {
		return factory;
	}
	
	private DiscountFactory() {
		discountTypeToEvalMapping = new HashMap<>();
		init();
	}
	
	/**
	 * Initialize Discount type to discount evaluator mapping
	 */
	private void init() {
		discountTypeToEvalMapping.put(DiscountType.PERCENT_DISCOUNT, DiscountEvaluators.FLAT_PERCENT_DISCOUNT);
		discountTypeToEvalMapping.put(DiscountType.EVERY_DOLLAR_DISCOUNT, DiscountEvaluators.EVERY_DOLLAR_DISCOUNT);
		discountTypeToEvalMapping.put(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE, DiscountEvaluators.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE);
	}
	
	public IDiscount<Order> getDiscountEval(DiscountType type) {
		return discountTypeToEvalMapping.get(type);
	}
}
