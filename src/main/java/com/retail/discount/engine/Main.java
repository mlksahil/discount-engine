package com.retail.discount.engine;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.retail.discount.engine.condition.ConditionDefinition;
import com.retail.discount.engine.core.IRule;
import com.retail.discount.engine.core.IRuleEngine;
import com.retail.discount.engine.dto.Discount;
import com.retail.discount.engine.dto.Item;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.dto.User;
import com.retail.discount.engine.enums.DiscountType;
import com.retail.discount.engine.enums.ItemType;
import com.retail.discount.engine.enums.UserType;
import com.retail.discount.engine.rule.Rule;
import com.retail.discount.engine.rule.RuleEngine;
import com.retail.discount.engine.rule.RuleFactory;

/**
 * Test Rules as per requirements. As such this class is not required.
 * 
 * @author Sahil Malik
 */
public class Main {
	public static void main(String[] args) {
		init();
		IRuleEngine ruleEngine = new RuleEngine();
		Order order = getOrder(UserType.EMPLOYEE);
		
		ruleEngine.applyDiscount(order);
		
		System.out.println("Sub-Total: " + order.getSubTotal());
		System.out.println("Discount: " + order.getDiscount());
		System.out.println("Total: " + order.getTotal());
	}

	private static void init() {
		List<IRule<Order>> ar = new ArrayList<>();
		
		// 1.	If the user is an employee of the store, he gets a 30% discount
		IRule<Order> rule1 = new Rule();
		rule1.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEmpDiscount());
		ar.add(rule1);
		
		// 2.	If the user is an affiliate of the store, he gets a 10% discount
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.USER_AFFILIATE, getAffDiscount());
		ar.add(rule2);
		
		// 3.	If the user has been a customer for over 2 years, he gets a 5% discount
		IRule<Order> rule3 = new Rule();
		rule3.ruleDefinition(ConditionDefinition.USER_X_YRS_OLD, getOldCustomerDiscount());
		ar.add(rule3);
		
		// 4.	For every $100 on the bill, there would be a $ 5 discount (e.g. for $ 990, you get $ 45 as a discount).
		IRule<Order> rule4 = new Rule();
		rule4.ruleDefinition(ConditionDefinition.ALWAYS_TRUE, getEveryDollarDiscount());
		ar.add(rule4);
		
		RuleFactory.getRuleFactory().init(ar);
	}
	
	private static Order getOrder(UserType type) {
		Order o = new Order();
		o.setBuyer(getUser());
		o.setId(1L);
		o.getBuyer().setUserType(type);
		o.setCart(getCart());
		o.setSubTotal(new BigDecimal(600));
		return o;
	}
	
	private static User getUser() {
		User user = new User();
		user.setEmail("abc@abc.com");
		user.setExternalId("123");
		user.setId(1L);
		user.setName("Sahil");
		user.setPhoneNo("123");
		Calendar registrationDate = Calendar.getInstance();
		registrationDate.add(Calendar.YEAR, -3);
		user.setRegistrationDate(registrationDate);
		return user;
	}
	
	private static List<Item> getCart() {
		List<Item> cart = new ArrayList<>();
		cart.add(new Item(1L,"A",ItemType.GROCERY,new BigDecimal(100)));
		cart.add(new Item(2L,"B",ItemType.HOME_KITCHEN,new BigDecimal(300)));
		cart.add(new Item(3L,"C",ItemType.HOUSE_HOLD_NEEDS,new BigDecimal(200)));
		return cart;
	}
	
	private static Discount getEmpDiscount() {
		Discount d = new Discount();
		d.setId(1L);
		d.setName("30% discount for employee");
		d.setDiscountValue(new BigDecimal(0.3));
		d.setDiscountType(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE);
		return d;
	}
	
	private static Discount getAffDiscount() {
		Discount d = new Discount();
		d.setId(2L);
		d.setName("20% discount for employee");
		d.setDiscountValue(new BigDecimal(0.2));
		d.setDiscountType(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE);
		return d;
	}
	
	private static Discount getOldCustomerDiscount() {
		Discount d = new Discount();
		d.setId(3L);
		d.setName("5% discount for employee");
		d.setDiscountValue(new BigDecimal(0.05));
		d.setDiscountType(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE);
		return d;
	}
	
	private static Discount getEveryDollarDiscount() {
		Discount d = new Discount();
		d.setId(4L);
		d.setName("every $100 on the bill, there would be a $ 5 discount ");
		d.setDiscountValue(new BigDecimal(5));
		d.setDiscountInterval(new BigDecimal(100));
		d.setDiscountType(DiscountType.EVERY_DOLLAR_DISCOUNT);
		return d;
	}
}