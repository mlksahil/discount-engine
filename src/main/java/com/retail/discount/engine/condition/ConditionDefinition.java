package com.retail.discount.engine.condition;

import java.io.Serializable;
import java.util.Calendar;

import org.mvel2.MVEL;

import com.retail.discount.engine.core.ICondition;
import com.retail.discount.engine.dto.Order;

/**
 * Dynamic Condition Evaluator
 * NOTE: Benefit of using MVEL is, condition string can be externalize to a DB and condition can be made dynamic.
 * 
 * @author Sahil Malik
 *
 */
public interface ConditionDefinition {

	//Condition to check if user is employee
	public ICondition<Order> USER_EMPLOYEE = (Order o) -> evaluator(o,"buyer.userType.name.equals(\"EMPLOYEE\")");
	// Condition to check if user is affiliate
	public ICondition<Order> USER_AFFILIATE = (Order o) -> evaluator(o,"buyer.userType.name.equals(\"AFFILIATE\")");
	// Condition to check if user's registration date is 2 yrs old
	public ICondition<Order> USER_X_YRS_OLD = (Order o) -> {
		Calendar twoYrsOldDate = Calendar.getInstance();
		// TODO Hard coded to 2 years for now. This configuration should be configured in condition attributes in DB and added here dynamically for evaluation.
		twoYrsOldDate.add(Calendar.YEAR, -2);
		long pastTime = twoYrsOldDate.getTimeInMillis();
		return evaluator(o,"buyer.registrationDate.timeInMillis < " + pastTime);
	};
	// Always true
	public ICondition<Order> ALWAYS_TRUE = (Order o) -> true;

	/**
	 * Static Method to evaluate Condition Expression using MVEL
	 * 
	 * @param order Order
	 * @param expression Expression
	 * @return True if expression is good
	 */
	public static boolean evaluator(Order order, String expression) {
		try {
			Serializable mvelExpression = MVEL.compileExpression(expression);
			return MVEL.executeExpression(mvelExpression, order, Boolean.class);
		} catch (Exception e) {
			System.err.println("Error evaluating Rule " + e);
			throw e;
		}
	}
}
