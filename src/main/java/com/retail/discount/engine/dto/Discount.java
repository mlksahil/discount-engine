package com.retail.discount.engine.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.retail.discount.engine.enums.DiscountType;

/**
 * 
 * Discount Object
 * 
 * @author Sahil Malik
 *
 */
public class Discount implements Serializable {
	private static final long serialVersionUID = -3375956176363962704L;
	private Long id;
	private String name;
	private String desc;
	private DiscountType discountType;
	private BigDecimal discountValue;
	private BigDecimal discountInterval;
	private BigDecimal maxDiscountValue;
	private Map<String,BigDecimal> attributes;
	
	/**
	 * @return the maxDiscountValue
	 */
	public BigDecimal getMaxDiscountValue() {
		return maxDiscountValue;
	}
	/**
	 * @param maxDiscountValue the maxDiscountValue to set
	 */
	public void setMaxDiscountValue(BigDecimal maxDiscountValue) {
		this.maxDiscountValue = maxDiscountValue;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	/**
	 * @return the discountType
	 */
	public DiscountType getDiscountType() {
		return discountType;
	}
	/**
	 * @param discountType the discountType to set
	 */
	public void setDiscountType(DiscountType discountType) {
		this.discountType = discountType;
	}
	/**
	 * @return the discountValue
	 */
	public BigDecimal getDiscountValue() {
		return discountValue;
	}
	/**
	 * @param discountValue the discountValue to set
	 */
	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}
	/**
	 * @return the discountInterval
	 */
	public BigDecimal getDiscountInterval() {
		return discountInterval;
	}
	/**
	 * @param discountInterval the discountInterval to set
	 */
	public void setDiscountInterval(BigDecimal discountInterval) {
		this.discountInterval = discountInterval;
	}
	/**
	 * @return the attributes
	 */
	public Map<String, BigDecimal> getAttributes() {
		return attributes;
	}
	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(Map<String, BigDecimal> attributes) {
		this.attributes = attributes;
	}
	
	/** (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributes == null) ? 0 : attributes.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((discountInterval == null) ? 0 : discountInterval.hashCode());
		result = prime * result + ((discountType == null) ? 0 : discountType.hashCode());
		result = prime * result + ((discountValue == null) ? 0 : discountValue.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((maxDiscountValue == null) ? 0 : maxDiscountValue.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	/** (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Discount other = (Discount) obj;
		if (attributes == null) {
			if (other.attributes != null)
				return false;
		} else if (!attributes.equals(other.attributes))
			return false;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		if (discountInterval == null) {
			if (other.discountInterval != null)
				return false;
		} else if (!discountInterval.equals(other.discountInterval))
			return false;
		if (discountType != other.discountType)
			return false;
		if (discountValue == null) {
			if (other.discountValue != null)
				return false;
		} else if (!discountValue.equals(other.discountValue))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (maxDiscountValue == null) {
			if (other.maxDiscountValue != null)
				return false;
		} else if (!maxDiscountValue.equals(other.maxDiscountValue))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Discount [id=" + id + ", name=" + name + ", desc=" + desc + ", discountType=" + discountType
				+ ", discountValue=" + discountValue + ", discountInterval=" + discountInterval + ", maxDiscountValue="
				+ maxDiscountValue + ", attributes=" + attributes + "]";
	}
}
