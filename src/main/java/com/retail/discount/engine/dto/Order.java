/**
 * 
 */
package com.retail.discount.engine.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Order Object
 * 
 * @author Sahil Malik
 *
 */
public class Order implements Serializable {

	private static final long serialVersionUID = -379850863448560376L;
	private Long id;
	private User buyer;
	private List<Item> cart;
	private BigDecimal subTotal = new BigDecimal(0);
	private BigDecimal total = new BigDecimal(0);
	private BigDecimal discount = new BigDecimal(0);
	private Discount appliedDiscount;
	
	public Order() {
		super();
		cart = new ArrayList<>();
	}

	public void addItemToCart(Item item) {
		cart.add(item);
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the buyer
	 */
	public User getBuyer() {
		return buyer;
	}

	/**
	 * @param buyer the buyer to set
	 */
	public void setBuyer(User buyer) {
		this.buyer = buyer;
	}

	/**
	 * @return the cart
	 */
	public List<Item> getCart() {
		return cart;
	}

	/**
	 * @param cart the cart to set
	 */
	public void setCart(List<Item> cart) {
		this.cart = cart;
	}

	/**
	 * @return the subTotal
	 */
	public BigDecimal getSubTotal() {
		return subTotal;
	}

	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	/**
	 * @return the discount
	 */
	public BigDecimal getDiscount() {
		return discount;
	}

	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	/**
	 * @return the appliedDiscount
	 */
	public Discount getAppliedDiscount() {
		return appliedDiscount;
	}

	/**
	 * @param appliedDiscount the appliedDiscount to set
	 */
	public void setAppliedDiscount(Discount appliedDiscount) {
		this.appliedDiscount = appliedDiscount;
	}

	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Order [id=" + id + ", buyer=" + buyer + ", cart=" + cart + ", subTotal=" + subTotal + ", total=" + total
				+ ", discount=" + discount + ", appliedDiscount=" + appliedDiscount + "]";
	}

	/** (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appliedDiscount == null) ? 0 : appliedDiscount.hashCode());
		result = prime * result + ((buyer == null) ? 0 : buyer.hashCode());
		result = prime * result + ((cart == null) ? 0 : cart.hashCode());
		result = prime * result + ((discount == null) ? 0 : discount.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((subTotal == null) ? 0 : subTotal.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		return result;
	}

	/** (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (appliedDiscount == null) {
			if (other.appliedDiscount != null)
				return false;
		} else if (!appliedDiscount.equals(other.appliedDiscount))
			return false;
		if (buyer == null) {
			if (other.buyer != null)
				return false;
		} else if (!buyer.equals(other.buyer))
			return false;
		if (cart == null) {
			if (other.cart != null)
				return false;
		} else if (!cart.equals(other.cart))
			return false;
		if (discount == null) {
			if (other.discount != null)
				return false;
		} else if (!discount.equals(other.discount))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (subTotal == null) {
			if (other.subTotal != null)
				return false;
		} else if (!subTotal.equals(other.subTotal))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		return true;
	}
	
	
	
}
