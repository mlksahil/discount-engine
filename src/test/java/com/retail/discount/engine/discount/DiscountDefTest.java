package com.retail.discount.engine.discount;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.math.BigDecimal;

import org.junit.Test;

import com.retail.discount.engine.AbstractTestHelper;
import com.retail.discount.engine.dto.Discount;
import com.retail.discount.engine.dto.Order;

/**
 * Discount Test 
 * 
 * @author Sahil Malik
 *
 */
public class DiscountDefTest extends AbstractTestHelper {
	
	@Test
	public void testDiscountPercentSuccess() {
		Order order = getOrder();
		order.setSubTotal(new BigDecimal(100.00));
		Discount discount = getDiscount();
		discount.setDiscountValue(new BigDecimal(0.3));
		assertEquals(DiscountEvaluators.FLAT_PERCENT_DISCOUNT.calculateDiscount(order, discount), new BigDecimal(30));
	}
	
	@Test
	public void testDiscountPercentFail() {
		Order order = getOrder();
		order.setSubTotal(new BigDecimal(100.00));
		Discount discount = getDiscount();
		discount.setDiscountValue(new BigDecimal(0.3));
		assertNotEquals(DiscountEvaluators.FLAT_PERCENT_DISCOUNT.calculateDiscount(order, discount), new BigDecimal(20));
	}
	
	@Test
	public void testDiscountIntervalSuccess() {
		Order order = getOrder();
		order.setSubTotal(new BigDecimal(990.00));
		Discount discount = getDiscount();
		discount.setDiscountValue(new BigDecimal(5));
		discount.setDiscountInterval(new BigDecimal(100));
		assertEquals(DiscountEvaluators.EVERY_DOLLAR_DISCOUNT.calculateDiscount(order, discount), new BigDecimal(45));
	}
	
	@Test
	public void testDiscountIntervalFail() {
		Order order = getOrder();
		order.setSubTotal(new BigDecimal(990.00));
		Discount discount = getDiscount();
		discount.setDiscountValue(new BigDecimal(5));
		discount.setDiscountInterval(new BigDecimal(100));
		assertNotEquals(DiscountEvaluators.EVERY_DOLLAR_DISCOUNT.calculateDiscount(order, discount), new BigDecimal(30));
	}
	
	@Test
	public void testDiscountPercentExcludingGrocerySuccess() {
		Order order = getOrder();
		order.setCart(getCart());
		order.setSubTotal(new BigDecimal(600.00));
		Discount discount = getDiscount();
		discount.setDiscountValue(new BigDecimal(0.3));
		assertEquals(DiscountEvaluators.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE.calculateDiscount(order, discount), new BigDecimal(150));
	}
	
	@Test
	public void testDiscountPercentExcludingGroceryFail() {
		Order order = getOrder();
		order.setCart(getCart());
		order.setSubTotal(new BigDecimal(600.00));
		Discount discount = getDiscount();
		discount.setDiscountValue(new BigDecimal(0.4));
		assertNotEquals(DiscountEvaluators.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE.calculateDiscount(order, discount), new BigDecimal(150));
	}
}
