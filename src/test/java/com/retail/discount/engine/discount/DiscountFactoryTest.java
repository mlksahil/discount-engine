package com.retail.discount.engine.discount;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.retail.discount.engine.AbstractTestHelper;
import com.retail.discount.engine.enums.DiscountType;

/**
 * Test Discount Factory.
 * 
 * @author Sahil Malik
 *
 */
public class DiscountFactoryTest extends AbstractTestHelper {
	
	@Test
	public void testDiscountFactory() {
		DiscountFactory factory = DiscountFactory.getInstance();
		assertEquals(DiscountEvaluators.FLAT_PERCENT_DISCOUNT, factory.getDiscountEval(DiscountType.PERCENT_DISCOUNT));
		assertEquals(DiscountEvaluators.EVERY_DOLLAR_DISCOUNT, factory.getDiscountEval(DiscountType.EVERY_DOLLAR_DISCOUNT));
		assertEquals(DiscountEvaluators.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE, factory.getDiscountEval(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE));
	}
}
