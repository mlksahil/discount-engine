package com.retail.discount.engine.condition;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import com.retail.discount.engine.AbstractTestHelper;
import com.retail.discount.engine.condition.ConditionDefinition;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.enums.UserType;  

/**
 * 
 * Condition Test
 * 
 * @author Sahil Malik
 *
 */
public class ConditionDefTest extends AbstractTestHelper {
	
	@Test
	public void testUserEmpSuccessCondition() {
		Order order = getOrder();
		order.getBuyer().setUserType(UserType.EMPLOYEE);
		assertTrue(ConditionDefinition.USER_EMPLOYEE.evaluate(order));
	}
	
	@Test
	public void testUserEmpFailCondition() {
		Order order = getOrder();
		order.getBuyer().setUserType(UserType.CUSTOMER);
		assertFalse(ConditionDefinition.USER_EMPLOYEE.evaluate(order));
	}
	
	@Test
	public void testUserAffSuccessCondition() {
		Order order = getOrder();
		order.getBuyer().setUserType(UserType.AFFILIATE);
		assertTrue(ConditionDefinition.USER_AFFILIATE.evaluate(order));
	}
	
	@Test
	public void testUserAffFailCondition() {
		Order order = getOrder();
		order.getBuyer().setUserType(UserType.CUSTOMER);
		assertFalse(ConditionDefinition.USER_AFFILIATE.evaluate(order));
	}
	
	@Test
	public void testUser2yrsSuccessCondition() {
		Order order = getOrder();
		order.getBuyer().setUserType(UserType.EMPLOYEE);
		Calendar instance = Calendar.getInstance();
		instance.add(Calendar.YEAR, -3);
		order.getBuyer().setRegistrationDate(instance);
		assertTrue(ConditionDefinition.USER_X_YRS_OLD.evaluate(order));
	}
	
	@Test
	public void testUser2yrsFailCondition() {
		Order order = getOrder();
		order.getBuyer().setUserType(UserType.EMPLOYEE);
		Calendar instance = Calendar.getInstance();
		instance.add(Calendar.YEAR, -1);
		order.getBuyer().setRegistrationDate(instance);
		assertFalse(ConditionDefinition.USER_X_YRS_OLD.evaluate(order));
	}
}
