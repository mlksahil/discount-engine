package com.retail.discount.engine.rule;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.retail.discount.engine.AbstractTestHelper;
import com.retail.discount.engine.condition.ConditionDefinition;
import com.retail.discount.engine.core.IRule;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.enums.DiscountType;

/**
 * Test Rule Factory.
 * 
 * @author Sahil Malik
 *
 */
public class RuleFactoryTest extends AbstractTestHelper {

	@Before
	public void init() {
		List<IRule<Order>> ar = new ArrayList<>();

		// 1. If the user is an employee of the store, he gets a 30% discount
		IRule<Order> rule1 = new Rule();
		rule1.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEmpDiscount());
		ar.add(rule1);

		// 2. If the user is an affiliate of the store, he gets a 10% discount
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.USER_AFFILIATE, getAffDiscount());
		ar.add(rule2);

		// 3. If the user has been a customer for over 2 years, he gets a 5%
		// discount
		IRule<Order> rule3 = new Rule();
		rule3.ruleDefinition(ConditionDefinition.USER_X_YRS_OLD, getOldCustomerDiscount());
		ar.add(rule3);

		// 4. For every $100 on the bill, there would be a $ 5 discount (e.g.
		// for $ 990, you get $ 45 as a discount).
		IRule<Order> rule4 = new Rule();
		rule4.ruleDefinition(ConditionDefinition.ALWAYS_TRUE, getEveryDollarDiscount());
		ar.add(rule4);

		RuleFactory.getRuleFactory().init(ar);
	}

	@Test
	public void testInitFactory() {
		Map<DiscountType, List<IRule<Order>>> ruleGroup = RuleFactory.getRuleFactory().getRuleGroup();
		assertEquals(3, ruleGroup.get(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE).size());
		assertEquals(1, ruleGroup.get(DiscountType.EVERY_DOLLAR_DISCOUNT).size());
	}

	@Test
	public void testClearFactory() {
		RuleFactory.getRuleFactory().clear();
		Map<DiscountType, List<IRule<Order>>> ruleGroup = RuleFactory.getRuleFactory().getRuleGroup();
		assertEquals(0, ruleGroup.size());
	}

}
