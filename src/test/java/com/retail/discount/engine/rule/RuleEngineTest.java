package com.retail.discount.engine.rule;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.retail.discount.engine.AbstractTestHelper;
import com.retail.discount.engine.condition.ConditionDefinition;
import com.retail.discount.engine.core.IRule;
import com.retail.discount.engine.core.IRuleEngine;
import com.retail.discount.engine.dto.Discount;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.enums.DiscountType;
import com.retail.discount.engine.enums.UserType;

/**
 * Rule Engine Test.
 * 
 * @author Sahil Malik
 *
 */
public class RuleEngineTest extends AbstractTestHelper {
	private Order order;
	private IRuleEngine ruleEngine = new RuleEngine();

	@Before
	public void testInit() {
		order = getOrder();
		order.setCart(getCart());
		order.setSubTotal(new BigDecimal(600));
	}
	
	@After
	public void clear() {
		RuleFactory.getRuleFactory().clear();;
	}

	/**
	 * If the user is an employee of the store, he gets a 30% discount
	 */
	@Test
	public void testUserEmpDiscountEvalSuccess() {
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule1 = new Rule();
		rule1.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEmpDiscount());
		ar.add(rule1);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.EMPLOYEE);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(450), order.getTotal());
	}

	/**
	 * If the user is an employee of the store, he gets a 30% discount - Fail
	 */
	@Test
	public void testUserEmpDiscountEvalFail() {
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule1 = new Rule();
		rule1.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEmpDiscount());
		ar.add(rule1);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.ADMIM);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(600), order.getTotal());
	}
	
	/**
	 * If the user is an affiliate of the store, he gets a 10% discount
	 */
	@Test
	public void testUserAffDiscountEvalSuccess() {
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.USER_AFFILIATE, getAffDiscount());
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.AFFILIATE);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(550), order.getTotal());
	}

	/**
	 * If the user is an affiliate of the store, he gets a 10% discount - Fail
	 */
	@Test
	public void testUserAffDiscountEvalFail() {
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.USER_AFFILIATE, getAffDiscount());
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.ADMIM);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(600), order.getTotal());
	}

	/**
	 * If the user has been a customer for over 2 years, he gets a 5% discount
	 */
	@Test
	public void testUserOldDiscountEvalSuccess() {
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.USER_X_YRS_OLD, getOldCustomerDiscount());
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.CUSTOMER);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(575), order.getTotal());
	}

	/**
	 * If the user has been a customer for over 2 years, he gets a 5% discount - Fail
	 */
	@Test
	public void testUserOldDiscountEvalFail() {
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.USER_AFFILIATE, getOldCustomerDiscount());
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);

		order.getBuyer().setUserType(UserType.CUSTOMER);
		Calendar registrationDate = Calendar.getInstance();
		registrationDate.add(Calendar.YEAR, -1);
		order.getBuyer().setRegistrationDate(registrationDate);
		
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(600), order.getTotal());
	}
	
	/**
	 * If the user has been a customer for over 2 years, he gets a 5% discount
	 */
	@Test
	public void testUserDollarDiscountEvalSuccess() {
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.ALWAYS_TRUE, getEveryDollarDiscount());
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.CUSTOMER);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(570), order.getTotal());
	}

	/**
	 * If the user has been a customer for over 2 years, he gets a 5% discount - Fail
	 */
	@Test
	public void testUserDollarDiscountEvalFail() {
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEveryDollarDiscount());
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.CUSTOMER);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(600), order.getTotal());
	}
	
	/**
	 * If there are multiple percentage discount, user will get only one which is of high value. 
	 * Here user registration date is 3 year old and also an employee.
	 */
	@Test
	public void testUserMultiDiscountEvalSuccess() {
		List<IRule<Order>> ar = new ArrayList<>();
		
		IRule<Order> rule1 = new Rule();
		rule1.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEmpDiscount());
		ar.add(rule1);
		
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.USER_X_YRS_OLD, getOldCustomerDiscount());
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.EMPLOYEE);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(450), order.getTotal());
	}
	
	/**
	 * User getting both percentage and dollar discount. Here user is an employee and does shopping for $600. He will get both 30% and $5 on every $100 discount
	 * Here user registration date is 3 year old and also an employee.
	 */
	@Test
	public void testUserPercentDollarDiscountEvalSuccess() {
		List<IRule<Order>> ar = new ArrayList<>();
		
		IRule<Order> rule1 = new Rule();
		rule1.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEmpDiscount());
		ar.add(rule1);
		
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.ALWAYS_TRUE, getEveryDollarDiscount());
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.EMPLOYEE);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(420), order.getTotal());
	}
	
	/**
	 * 
	 * User should get percent discount which is of higher value. Here, user registration date is more than 2 years and also an employee.
	 * 
	 */
	@Test
	public void testUserPercentDollarDiscount2EvalSuccess() {
		List<IRule<Order>> ar = new ArrayList<>();
		
		IRule<Order> rule1 = new Rule();
		rule1.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEmpDiscount());
		ar.add(rule1);
		
		Discount d = new Discount();
		d.setId(3L);
		d.setName("50% discount for employee");
		d.setDiscountValue(new BigDecimal(0.5));
		d.setDiscountType(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE);
		
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.ALWAYS_TRUE, d);
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.EMPLOYEE);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(350), order.getTotal());
	}
	
	/**
	 * Test Exception
	 */
	@Test(expected = Exception.class)
	public void testError() throws Exception {
		List<IRule<Order>> ar = new ArrayList<>();
		order.getBuyer().setUserType(null);
		IRule<Order> rule1 = new Rule();
		rule1.ruleDefinition(ConditionDefinition.USER_EMPLOYEE, getEmpDiscount());
		ar.add(rule1);
		RuleFactory.getRuleFactory().init(ar);
		
		try {
			ruleEngine.applyDiscount(order);
		} catch (Exception e) {
			throw new Exception(e);
		}
		assertEquals(new BigDecimal(0), order.getDiscount());
		assertEquals(order.getSubTotal(), order.getTotal());
	}
	
	/**
	 * If the user has been a customer for over 2 years, he gets a 5% discount
	 */
	@Test
	public void testDiscountInvalidConfEvalSuccess() {
		
		Discount d = new Discount();
		d.setId(4L);
		d.setName("every $100 on the bill, there would be a $ 110 discount ");
		d.setDiscountValue(new BigDecimal(110));
		d.setDiscountInterval(new BigDecimal(100));
		d.setDiscountType(DiscountType.EVERY_DOLLAR_DISCOUNT);
		
		List<IRule<Order>> ar = new ArrayList<>();
		IRule<Order> rule2 = new Rule();
		rule2.ruleDefinition(ConditionDefinition.ALWAYS_TRUE, d);
		ar.add(rule2);
		RuleFactory.getRuleFactory().init(ar);
		
		order.getBuyer().setUserType(UserType.CUSTOMER);
		ruleEngine.applyDiscount(order);
		assertEquals(new BigDecimal(0), order.getTotal());
	}

}