package com.retail.discount.engine;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.retail.discount.engine.dto.Discount;
import com.retail.discount.engine.dto.Item;
import com.retail.discount.engine.dto.Order;
import com.retail.discount.engine.dto.User;
import com.retail.discount.engine.enums.DiscountType;
import com.retail.discount.engine.enums.ItemType;

/**
 * Helper class - Use to create mock object for testing.
 * 
 * @author Sahil Malik
 *
 */
public abstract class AbstractTestHelper {
	
	protected Order getOrder() {
		Order o = new Order();
		o.setBuyer(getUser());
		o.setId(1L);
		return o;
	}
	
	protected User getUser() {
		User user = new User();
		user.setEmail("abc@abc.com");
		user.setExternalId("123");
		user.setId(1L);
		user.setName("Sahil");
		user.setPhoneNo("123");
		Calendar registrationDate = Calendar.getInstance();
		registrationDate.add(Calendar.YEAR, -3);
		user.setRegistrationDate(registrationDate);
		return user;
	}
	
	protected Discount getDiscount() {
		Discount discount = new Discount();
		discount.setId(1L);
		discount.setName("Discount");
		discount.setDesc("Description");
		return discount;
	}

	protected List<Item> getCart() {
		List<Item> cart = new ArrayList<>();
		cart.add(new Item(1L,"A",ItemType.GROCERY,new BigDecimal(100)));
		cart.add(new Item(2L,"B",ItemType.HOME_KITCHEN,new BigDecimal(300)));
		cart.add(new Item(3L,"C",ItemType.HOUSE_HOLD_NEEDS,new BigDecimal(200)));
		return cart;
	}
	
	protected Discount getEmpDiscount() {
		Discount d = new Discount();
		d.setId(1L);
		d.setName("30% discount for employee");
		d.setDiscountValue(new BigDecimal(0.3));
		d.setDiscountType(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE);
		return d;
	}
	
	protected Discount getAffDiscount() {
		Discount d = new Discount();
		d.setId(2L);
		d.setName("10% discount for employee");
		d.setDiscountValue(new BigDecimal(0.1));
		d.setDiscountType(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE);
		return d;
	}
	
	protected Discount getOldCustomerDiscount() {
		Discount d = new Discount();
		d.setId(3L);
		d.setName("5% discount for employee");
		d.setDiscountValue(new BigDecimal(0.05));
		d.setDiscountType(DiscountType.PERCENT_DISCOUNT_EXCLUDING_ITEM_TYPE);
		return d;
	}
	
	protected Discount getEveryDollarDiscount() {
		Discount d = new Discount();
		d.setId(4L);
		d.setName("every $100 on the bill, there would be a $ 5 discount ");
		d.setDiscountValue(new BigDecimal(5));
		d.setDiscountInterval(new BigDecimal(100));
		d.setDiscountType(DiscountType.EVERY_DOLLAR_DISCOUNT);
		return d;
	}
}